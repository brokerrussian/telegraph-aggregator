const $validtor = require('jsonschema').validate;
const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const moduleName = 'handlers.list';

const _ = require('lodash');

const SCHEMA = require('../schemes/list');
const ERRORS = require('../../../dtp-api/src/references/errors');

module.exports = function (app) {
  app.post('/list/:access_token', (req, res) => {
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    const telegraphToken = req.params.access_token;

    $Posts.list(telegraphToken, req.body)
      .then((result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (_.get(err, 'error') === 'ACCESS_TOKEN_INVALID') {
          return res.json({
            ok: false,
            error: ERRORS['denied-account']
          });
        }

        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};