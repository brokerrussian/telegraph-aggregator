const Schema = require('../../../dtp-api/config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "offset": {
      "type": "integer",
      "minimum": 0
    },
    "query": Schema.anyString
  },
  "required": []
};